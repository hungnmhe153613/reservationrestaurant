/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import jdbc.core.JdbcTemplate;
import model.Dishes;
import model.table_1;


/**
 *
 * @author Hung
 */
public class DishesDao extends DBContext{
    JdbcTemplate jdbcTemplate = new JdbcTemplate();
          public List<Dishes> getAll() {
        String sql = "SELECT * FROM Dishes";

        try (
             PreparedStatement ps = connection.prepareStatement(sql)) {
            return jdbcTemplate.queryForList(ps, Dishes.class);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return null;
    
}
          
          public void addmenu(Dishes d){
              try {
                  String sql = "insert into dishes values (?, ?, 'dish.jpg', ?, ?)";
                  PreparedStatement stm = connection.prepareStatement(sql);
                  stm.setString(1, d.getName());
                  stm.setDouble(2, d.getPrice());
                  stm.setString(3, d.getDescription());
                  stm.setInt(4, d.getStatus());
                  stm.executeUpdate();
              } catch (SQLException e) {
              }
          }
          
   
            public void update(Dishes t){
        try {
            String sql = "update dishes set price=?,[description] = ?,[status]=?, img_name=?, [name]=? where id=?;";
            
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setDouble(1, t.getPrice());
            stm.setString(2, t.getDescription());
            stm.setInt(3, t.getStatus());
            stm.setString(4, t.getImgName());
            stm.setString(5, t.getName());
            stm.setInt(6, t.getId());
            stm.executeUpdate();
        } catch (SQLException e) {
        }
    }
          
          public static void main(String[] args) {
              System.out.println(new DishesDao().getAll());
    }
                  
          
}
