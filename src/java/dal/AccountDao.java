/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Account;

/**
 *
 * @author Hung
 */
public class AccountDao extends DBContext{
    public Account checkLogin(String user, String pass){
        String sql = "Select * from Account where user_name = ? and password = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, user);
            stm.setString(2, pass);
            ResultSet rs = stm.executeQuery();
            while(rs.next()){
                Account a = new Account();
                a.setUsername(rs.getString(1));
                a.setUsername(rs.getString(2));
                a.setAdmin(rs.getInt(3));
                a.setGuest(rs.getInt(4));
                return a;
            }
        } catch (SQLException e) {
        }
        return null;
    }
    
     public void insert(String user, String pass){
        String sql = "insert into Account values (?, ?)";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, user);
            stm.setString(2, pass);
            stm.executeUpdate();
        } catch (SQLException e) {
        }
    }
   
    public Account checkUser(String user){
        String sql = "select user_name, password from Account where user_name = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, user);
            ResultSet rs = stm.executeQuery();
            while (rs.next()){
                Account account = new Account();
                account.setUsername(rs.getString("username"));
                account.setPassword(rs.getString("password"));
                return account;
            }    
        } catch (SQLException e) {
        }
        return null;
    }
    public static void main(String[] args) {
        System.out.println(new AccountDao().checkLogin("dragonkill255", "123456"));
    }
    
}
