/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.table_1;

/**
 *
 * @author Hung
 */
public class Table1Dao extends DBContext{
    public void update(table_1 t){
        try {
            String sql = "update table_1 set name = ?, person = ?, phone = ?, status = ?, day = ?,[email] = ? where [table] = ? and [time] = ?";
            
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, t.getName());
            stm.setInt(2, t.getPerson());
            stm.setString(3, t.getPhone());
            stm.setInt(4, 1);
            stm.setDate(5, t.getDate());
            stm.setString(6, t.getEmail());
            stm.setString(7, t.getTable());
            stm.setString(8, t.getTime());
            
            stm.executeUpdate();
        } catch (SQLException e) {
        }
    }
    
    public ArrayList<table_1> getAllbyStt(){
         ArrayList<table_1> list = new ArrayList<>();
        try {
           
            String sql = "Select * from table_1 where status = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                table_1 t = new table_1();
                t.setId(rs.getInt(1));
                t.setTable(rs.getString(2));
                t.setTime(rs.getString(3));
                t.setName(rs.getString(4));
                t.setPerson(rs.getInt(5));
                t.setPhone(rs.getString(6));
                t.setStatus(rs.getInt(7));
                t.setDate(rs.getDate(8));
                t.setEmail(rs.getString(9));
                
                list.add(t);
                
            }
        } catch (Exception e) {
        }
        return list;
    }
    
     public ArrayList<table_1> getAllUsedTable(){
         ArrayList<table_1> list = new ArrayList<>();
        try {
           
            String sql = "Select * from table_1 where status = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {                
                table_1 t = new table_1();
                t.setId(rs.getInt(1));
                t.setTable(rs.getString(2));
                t.setTime(rs.getString(3));
                t.setName(rs.getString(4));
                t.setPerson(rs.getInt(5));
                t.setPhone(rs.getString(6));
                t.setStatus(rs.getInt(7));
                t.setDate(rs.getDate(8));
                t.setEmail(rs.getString(9));
                
                list.add(t);
                
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public static void main(String[] args) {
        table_1 t = new table_1();
//        t.setTime(ResultSet.getparam("table"));
        t.setDate(Date.valueOf("2012-12-16"));
        t.setEmail("hieu@gmail.com");
        t.setName("hung");
        t.setStatus(0);
        t.setPerson(1);
        t.setTable("1");
        t.setPhone("26565");
        t.setTime("10:00-12:00");
        
//        System.out.println(t);
        new Table1Dao().update(t);
    }
    
      public ArrayList<table_1> getAllbyTable(String table){
         ArrayList<table_1> list = new ArrayList<>();
        try {
           
            String sql = "Select * from table_1 where status = 0 and [table]=?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, table);
            ResultSet rs = stm.executeQuery();
            
            while (rs.next()) {                
                table_1 t = new table_1();
                t.setId(rs.getInt(1));
                t.setTable(rs.getString(2));
                t.setTime(rs.getString(3));
                t.setName(rs.getString(4));
                t.setPerson(rs.getInt(5));
                t.setPhone(rs.getString(6));
                t.setStatus(rs.getInt(7));
                t.setDate(rs.getDate(8));
                t.setEmail(rs.getString(9));
                
                list.add(t);
                
            }
        } catch (Exception e) {
        }
        return list;
    }
         public void update2(int id){
        try {
            String sql = "update table_1 set name = 'x', person = 0, phone = 'x', status = 0, day = '2021-03-30',email ='x' where id=?";
            
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            
            
            stm.executeUpdate();
        } catch (SQLException e) {
        }
    }
         
     
  
      
}
