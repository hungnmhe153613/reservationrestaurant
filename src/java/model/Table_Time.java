/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public class Table_Time {
    private Booking table;
    private Time time;

    public Booking getTable() {
        return table;
    }

    public void setTable(Booking table) {
        this.table = table;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Table_Time() {
    }

    public Table_Time(Booking table, Time time) {
        this.table = table;
        this.time = time;
    }
    
}
