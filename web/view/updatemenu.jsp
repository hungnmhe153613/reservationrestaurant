<%-- 
    Document   : manage.jsp
    Created on : Mar 30, 2021, 6:29:13 PM
    Author     : Hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </head>
    <body>
        <c:if test="${sessionScope.auth==null}"><c:redirect url="AccessDenied.jsp"/></c:if>
        <h1 style="text-align: center">Booked Table</h1>
       
        <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Price</th>
      <th scope="col">Image</th>
      <th scope="col">Description</th>
      <th scope="col">Type</th>
      
      <th>Action</th>
    </tr>
  </thead>

  <tbody>
      
      <c:forEach items="${requestScope.lsDishes}" var="d">
          <tr>
          <td>${d.name}</td>
          <td>${d.price}</td>
          <td>${d.imgName}</td>
          <td>${d.description}</td>
          <c:if test="${d.status==1}"><td>Món chính</td></c:if>
          <c:if test="${d.status==2}"><td>Món khai vị</td></c:if>
          <c:if test="${d.status==3}"><td>Món tráng miệng</td></c:if>
          
          <td ><a href="updatemenu2?id=${d.id}" style="border: 1px solid black;text-decoration: none;background-color: green;color: white">Update</a>&nbsp; 
          
          
          
          </tr>
      </c:forEach>
    
  </tbody>
 
</table>
             


    </body>
</html>
