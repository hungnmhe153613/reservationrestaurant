<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Sam bà tỵ restaurant</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/hero-slider.css">
        <link rel="stylesheet" href="css/owl-carousel.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Spectral:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
               <div class="header">
            <div class="container">
                <a href="#" class="navbar-brand scroll-top">Book your table now</a>
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!--/.navbar-header-->
                    <div id="main-nav" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="home">Home</a></li>
                            <li><a href="menu">Our Menu</a></li>
                            <li><a href="https://www.facebook.com/dacsansamhalong">Social Media</a></li>
                            <li><a href="book1">Booking</a></li>
                            <c:if test="${sessionScope.auth==null}"><li><a href="login">Login</a></li></c:if>
                            <c:if test="${sessionScope.auth!=null}"><li><a href="admin">Admin page</a></li>&nbsp;<li><a href="logout">Logout</a></li></c:if>

                        </ul>
                    </div>
                    <!--/.navbar-collapse-->
                </nav>
                <!--/.navbar-->
            </div>
            <!--/.container-->
        </div>
        <section id="book-table">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <h2>Book Your Table Now</h2>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-2 col-sm-12">
                        <div class="left-image">
                            <img src="img/book_left_image.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="right-info">
                            <h4>Reservation</h4>
                            <form id="form-submit" action="booking" method="post">
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <fieldset>
                                            <div class="dropdown">
                                                <button class=" dropdown-toggle" type="button" data-toggle="dropdown">Table
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="booking?table=1">Ban 1</a></li>
                                                    <li><a href="booking?table=2">Ban 2</a></li>
                                                    <li><a href="booking?table=3">Ban 3</a></li>
                                                    <li><a href="booking?table=3">Ban 4</a></li>
                                                </ul>
                                            </div>
                                        </fieldset>
                                    </div>
                                 
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>Address: 188 group 19, block 2B, Cao Thang Ward, Ha Long City, Quang Ninh Province</p>
                </div>
                
                
                <div class="col-md-4">
                    <ul class="social-icons">
                        <li><a href="https://www.facebook.com/dacsansamhalong"><i class="fa fa-facebook"></i></a></li>
                        
                    </ul>
                </div>
                <div class="col-md-4">
                    <p>Designed by <em>Nguyen Minh Hung</em></p>
                </div>
            </div>
        </div>
    </footer>