<%-- 
    Document   : manage.jsp
    Created on : Mar 30, 2021, 6:29:13 PM
    Author     : Hung
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </head>
    <body>
        <c:if test="${sessionScope.auth==null}"><c:redirect url="AccessDenied.jsp"/></c:if>
        <h1 style="text-align: center">Booked Table</h1>
       
        <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Table</th>
      <th scope="col">Time</th>
      <th scope="col">Guest Name</th>
      <th scope="col">Phone Number</th>
      <th scope="col">Number Person</th>
      <th scope="col">Email</th>
      <th>Action</th>
    </tr>
  </thead>

  <tbody>
      
      <c:forEach items="${requestScope.use}" var="d">
          <tr>
          <td>${d.table}</td>
          <td>${d.time}</td>
          <td>${d.name}</td>
          <td>${d.phone}</td>
          <td>${d.person}</td>   
          <td>${d.email}</td>
          <td ><a href="update?id=${d.id}" style="border: 1px solid black;text-decoration: none;background-color: red;color: white">Reject</a>&nbsp; 
          <a href="send?table=${d.table}&time=${d.time}&email=${d.email}" style="border: 1px solid black;text-decoration: none;background-color: green;color: white">Accept</a>&nbsp;</td>
          
          
          </tr>
      </c:forEach>
    
  </tbody>
 
</table>
         <h1 style="text-align: center">Nonbooked table</h1>
        <br>     
 <table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">Table</th>
      <th scope="col">Time</th>
      <th scope="col">Guest Name</th>
      <th scope="col">Phone Number</th>
      <th scope="col">Number Person</th>
      <th scope="col">Email</th>
      <th>Action</th>
    </tr>
  </thead>

  <tbody>
      
      <c:forEach items="${requestScope.nonuse}" var="d">
          <tr>
          <td>${d.table}</td>
          <td>${d.time}</td>
          <td>${d.name}</td>
          <td>${d.phone}</td>
          <td>${d.person}</td>   
          <td>${d.email}</td>
          <td ><a href="book1" style="border: 1px solid black;text-decoration: none;background-color: red;color: white">Update</a>&nbsp;</td>                  
          </tr>
      </c:forEach>
    
  </tbody>
</table>

    </body>
</html>
