<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title>Sam bà tỵ restaurant</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">
        <link rel="stylesheet" href="css/hero-slider.css">
        <link rel="stylesheet" href="css/owl-carousel.css">
        <link rel="stylesheet" href="css/templatemo-style.css">

        <link href="https://fonts.googleapis.com/css?family=Spectral:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

    <body>
        <div class="header">
            <div class="container">
                <a href="#" class="navbar-brand scroll-top">Welcome to Sam Bà Tỵ</a>
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="navbar-header">
                        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!--/.navbar-header-->
                    <div id="main-nav" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="home">Home</a></li>
                            <li><a href="menu">Our Menu</a></li>
                            <li><a href="https://www.facebook.com/dacsansamhalong">Social Media</a></li>
                            <li><a href="book1">Booking</a></li>
                            <c:if test="${sessionScope.auth==null}"><li><a href="login">Login</a></li></c:if>
                            <c:if test="${sessionScope.auth!=null}"><li><a href="admin">Admin page</a></li>&nbsp;<li><a href="logout">Logout</a></li></c:if>

                        </ul>
                    </div>
                    <!--/.navbar-collapse-->
                </nav>
                <!--/.navbar-->
            </div>
            <!--/.container-->
        </div>
        <!--/.header-->


        <section class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h4>Here you can find delecious foods</h4>
                        <h2>Seafood Restaurant</h2>
                        <p>
                            With over 30 years of operation, we are proud to bring you the best experience</p>
                        <div class="primary-button">
                            <a href="book1" >Order Right Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="cook-delecious">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-md-offset-1">
                        <div class="first-image">
                            <img src="img/cook_01.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="cook-content">
                            <h4>We cook delecious</h4>
                            <div class="contact-content">
                                <span>You can call us on:</span>
                                <h6>0915411877</h6>
                            </div>
                            <span>or</span>
                            <div class="primary-white-button">
                                <a href="#" class="scroll-link" data-id="book-table">Order Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="second-image">
                            <img src="img/cook_02.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="services" style="margin-left: 250px">
            <div class="container">
                <div class="row" >
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="service-item">
                            <a href="menu">
                                <img src="img/cook_breakfast.png" alt="Appetizers">
                                <h4>Appetizers</h4>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="service-item">
                            <a href="menu">
                                <img src="img/cook_dinner.png" alt="Main Dishes">
                                <h4>Main Dishes</h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="service-item">
                            <a href="menu">
                                <img src="img/cook_dessert.png" alt="Desserts">
                                <h4>Desserts</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>




        <section class="featured-food">
            <div class="container">
                <div class="row">
                    <div class="heading">
                        <h2>Favorite Food</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="food-item">
                            <h2>Appetizers</h2>
                            <img src="img/nomsam.jpg" alt="">
                            <div class="price">50.000</div>
                            <div class="text-content">
                                <h4>The best appetizers</h4>
                                <p>Made from vegetable, so fresh and good for your heath</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="food-item">
                            <h2>Main Dishes</h2>
                            <img src="img/thitsam.jpg" alt="">
                            <div class="price">230.000</div>
                            <div class="text-content">
                                <h4>The best taste</h4>
                                <p>With a pineapple flavor, this dish gives you a fresh and unforgettable taste</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="food-item">
                            <h2>Desserts</h2>
                            <img src="img/cake.jpg" alt="">
                            <div class="price">50.000</div>
                            <div class="text-content">
                                <h4>Delicious cake</h4>
                                <p>It is so soft, bring a tender taste...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="our-blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <h2>Our blog post</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="blog-post">
                            <img src="img/blog_post_01.jpg" alt="">
                            <div class="date">31 Mar 2021</div>
                            <div class="right-content">
                                <h4>Foody Sam bà tỵ</h4>
                                <span>Branding / Admin</span>
                                <p>More about us in foody website...</p>
                                <div class="text-button">
                                    <a href="https://www.foody.vn/quang-ninh/sam-ba-ty">Continue Reading</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="blog-post">
                            <img src="img/blog_post_02.jpg" alt="">
                            <div class="date">31 Mar 2021</div>
                            <div class="right-content">
                                <h4>Mon ngon Sam Ba Ty</h4>
                                <span>Branding / Admin</span>
                                <p>We have more information about us...</p>
                                <div class="text-button">
                                    <a href="https://diachiquanngon.com/nha-hang/sam-ba-ty/">Continue Reading</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="blog-post">
                            <img src="img/blog_post_03.jpg" alt="">
                            <div class="date">31 Mar 2021</div>
                            <div class="right-content">
                                <h4>More about Ha Long food</h4>
                                <span>Dessert / Chef</span>
                                <p>information more about ha long food...</p>
                                <div class="text-button">
                                    <a href="https://justfly.vn/discovery/vietnam/ha-long/nhung-mon-dac-san-ha-long-ngon-kho-cuong-khong-the-bo-qua">Continue Reading</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="blog-post">
                            <img src="img/blog_post_04.jpg" alt="">
                            <div class="date">31 Mar 2021</div>
                            <div class="right-content">
                                <h4>Crucifix Selvage Coat</h4>
                                <span>Plate / Chef</span>
                                <p>Skateboard iceland twee tofu shaman crucifix vice before they sold out corn hole occupy drinking vinegar chambra meggings kale chips hexagon...</p>
                                <div class="text-button">
                                    <a href="#">Continue Reading</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="sign-up">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <h2>Signup for our newsletters</h2>
                        </div>
                    </div>
                </div>
                <form id="contact" action="" method="get">
                    <div class="row">
                        <div class="col-md-4 col-md-offset-3">
                            <fieldset>
                                <input name="email" type="text" class="form-control" id="email" placeholder="Enter your email here..." required>
                            </fieldset>
                        </div>
                        <div class="col-md-2">
                            <fieldset>
                                <button type="submit" id="form-submit" class="btn">Send Message</button>
                            </fieldset>
                        </div>
                    </div>
                </form>
            </div>
        </section>



    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <p>Address: 188 group 19, block 2B, Cao Thang Ward, Ha Long City, Quang Ninh Province</p>
                </div>
                
                
                <div class="col-md-4">
                    <ul class="social-icons">
                        <li><a href="https://www.facebook.com/dacsansamhalong"><i class="fa fa-facebook"></i></a></li>
                        
                    </ul>
                </div>
                <div class="col-md-4">
                    <p>Designed by <em>Nguyen Minh Hung</em></p>
                </div>
            </div>
        </div>
    </footer>





        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                // navigation click actions 
                $('.scroll-link').on('click', function (event) {
                    event.preventDefault();
                    var sectionID = $(this).attr("data-id");
                    scrollToID('#' + sectionID, 750);
                });
                // scroll to top action
                $('.scroll-top').on('click', function (event) {
                    event.preventDefault();
                    $('html, body').animate({scrollTop: 0}, 'slow');
                });
                // mobile nav toggle
                $('#nav-toggle').on('click', function (event) {
                    event.preventDefault();
                    $('#main-nav').toggleClass("open");
                });
            });
            // scroll function
            function scrollToID(id, speed) {
                var offSet = 0;
                var targetOffset = $(id).offset().top - offSet;
                var mainNav = $('#main-nav');
                $('html,body').animate({scrollTop: targetOffset}, speed);
                if (mainNav.hasClass("open")) {
                    mainNav.css("height", "1px").removeClass("in").addClass("collapse");
                    mainNav.removeClass("open");
                }
            }
            if (typeof console === "undefined") {
                console = {
                    log: function () { }
                };
            }
        </script>
    </body>
</html>