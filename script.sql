USE [ProjectPrj]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 8/11/2021 4:12:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[user_name] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[admin] [int] NOT NULL,
	[guest] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Booking]    Script Date: 8/11/2021 4:12:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Booking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[day] [date] NOT NULL,
	[table] [nvarchar](50) NOT NULL,
	[Time] [nvarchar](50) NOT NULL,
	[name] [text] NOT NULL,
	[person] [nvarchar](50) NOT NULL,
	[phone] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Booking_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[day] ASC,
	[table] ASC,
	[Time] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[boook]    Script Date: 8/11/2021 4:12:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[boook](
	[id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[person] [int] NOT NULL,
	[phone] [varchar](50) NOT NULL,
	[info] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dishes]    Script Date: 8/11/2021 4:12:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dishes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NOT NULL,
	[price] [float] NOT NULL,
	[img_name] [nvarchar](200) NOT NULL,
	[description] [nvarchar](2000) NOT NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_dishes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Table_1]    Script Date: 8/11/2021 4:12:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_1](
	[id] [int] NOT NULL,
	[table] [nvarchar](50) NOT NULL,
	[time] [nvarchar](50) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[person] [int] NOT NULL,
	[phone] [nvarchar](50) NOT NULL,
	[status] [int] NOT NULL,
	[day] [date] NOT NULL,
	[email] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([user_name], [password], [admin], [guest]) VALUES (N'dragonkill255', N'123456', 1, 0)
INSERT [dbo].[Account] ([user_name], [password], [admin], [guest]) VALUES (N'hung', N'123456', 1, 0)
GO
SET IDENTITY_INSERT [dbo].[Booking] ON 

INSERT [dbo].[Booking] ([id], [day], [table], [Time], [name], [person], [phone]) VALUES (1, CAST(N'2021-05-02' AS Date), N'bàn 1', N'10:00', N'hihi', N'oke', N'0921189960')
INSERT [dbo].[Booking] ([id], [day], [table], [Time], [name], [person], [phone]) VALUES (2, CAST(N'2021-09-15' AS Date), N'bàn 1', N'10:30', N'lala', N'1 person', N'0921189960')
INSERT [dbo].[Booking] ([id], [day], [table], [Time], [name], [person], [phone]) VALUES (3, CAST(N'2030-12-16' AS Date), N'bàn 3', N'10:21', N'hihi', N'2 person', N'0951313165')
SET IDENTITY_INSERT [dbo].[Booking] OFF
GO
SET IDENTITY_INSERT [dbo].[dishes] ON 

INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (3, N'Chân xào', 130000, N'chanxao.jpg', N'Mang hương vị truyền thống của sam, được nấu cùng lá nốt', 1)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (4, N'Chân sam rán', 140000, N'chanran.jpg', N'Được chế biến từ phần ngon nhất của con sam, sẽ mang đến cho bạn trải nghiệm khó quên', 1)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (5, N'Chả sam', 150000, N'chasam.jpg', N'Món này ngon lắm', 1)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (7, N'Nộm Sam', 50000, N'nomsam.jpg', N'Món khai vị độc đáo', 2)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (8, N'Trứng Sam', 50000, N'trungsam.jpg', N'Được chiên giòn vàng óng, đem lại cảm giác giòn tan trong miệng', 1)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (9, N'Thịt Sam xào dứa', 230000, N'thitsam.jpg', N'Thịt sam là phần độc đáo nhất của sam', 1)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (10, N'Riêu Sam', 50000, N'rieusam.jpg', N'Được nấu cùng với tiết sam, hương vị chua ngọt mang lại cho bạn một món ăn vô cùng độc đáo', 1)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (14, N'Sam Đôi', 1500000, N'samdoi.jpg', N'Bao gồm 7 món của sam', 1)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (16, N'Dưa chuột', 5000, N'duachuot.jpg', N'Dưa chuột là món ăn truyền thống phù hợp với khẩu vị của người Việt', 2)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (17, N'Củ Đậu', 5000, N'cudau.jpg', N'Củ đậu là món ăn khởi đầu tuyệt vời cho hệ dạ dày', 2)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (19, N'Dưa hấu', 15000, N'duahau.jpg', N'Dưa hấu rất tươi', 3)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (20, N'Bánh da heo', 15000, N'banhdaheo.jpg', N'bánh rất ngon', 3)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (21, N'Bánh plan', 75000, N'banhplan.jpg', N'Với hương vị chocolate, mang lại cảm giác ngọt ngào ....', 3)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (22, N'Tôm', 150000, N'dish.jpg', N'hihi', 1)
INSERT [dbo].[dishes] ([id], [name], [price], [img_name], [description], [status]) VALUES (25, N'Mực khô', 150000, N'muckho.jpg', N'Mực khô 1 nắng ', 2)
SET IDENTITY_INSERT [dbo].[dishes] OFF
GO
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (1, N'1', N'10:00-12:00', N'aaa', 4, N'0123456789', 1, CAST(N'2021-05-21' AS Date), N'nguyenphuonglinh1106@gmail.com')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (2, N'1', N'12:00-14:00', N'tuấn việt', 3, N'123456789', 1, CAST(N'2021-07-15' AS Date), N'halongpro102@gmail.com')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (3, N'1', N'14:00-16:00', N'tuấn việt', 2, N'123456', 1, CAST(N'2021-08-17' AS Date), N'longtnhe141662@fpt.edu.vn')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (4, N'2', N'10:00-12:00', N'unicorn', 4, N'332013556', 1, CAST(N'2022-12-09' AS Date), N'huyduc1092000@gmail.com')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (5, N'2', N'12:00-14:00', N'x', 0, N'x', 0, CAST(N'2021-03-30' AS Date), N'x')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (6, N'2', N'14:00-16:00', N'x', 0, N'0', 0, CAST(N'2021-03-30' AS Date), N'x')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (7, N'3', N'10:00-12:00', N'x', 0, N'x', 0, CAST(N'2021-03-30' AS Date), N'x')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (8, N'3', N'12:00-14:00', N'x', 0, N'0', 0, CAST(N'2021-03-30' AS Date), N'x')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (9, N'3', N'14:00-16:00', N'x', 0, N'0', 0, CAST(N'2021-03-30' AS Date), N'x')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (10, N'4', N'12:00-14:00', N'x', 0, N'0', 0, CAST(N'2021-03-30' AS Date), N'x')
INSERT [dbo].[Table_1] ([id], [table], [time], [name], [person], [phone], [status], [day], [email]) VALUES (11, N'4', N'10:00-12:00', N'x', 0, N'0', 0, CAST(N'2021-03-30' AS Date), N'x')
GO
